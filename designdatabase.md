# Database Schema

## Tables

### Users
- id: INT (Primary Key)
- username: VARCHAR
- email: VARCHAR
- password: VARCHAR

### Posts
- id: INT (Primary Key)
- title: VARCHAR
- content: TEXT
- user_id: INT (Foreign Key referencing Users.id)

### Comments
- id: INT (Primary Key)
- content: TEXT
- post_id: INT (Foreign Key referencing Posts.id)
- user_id: INT (Foreign Key referencing Users.id)

## Relationships

- Users (1) -> (Many) Posts
- Users (1) -> (Many) Comments
- Posts (1) -> (Many) Comments
